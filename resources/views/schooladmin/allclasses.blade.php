@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Classes</h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                        <button type="button" class="btn btn-primary" onclick="loadModal( '/register-new-class')" data-toggle="modal" data-target="#modal-lg">
                            Register New Class
                        </button>
                    </li>
                  </ul>
                </div>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php $counter = 1; ?>
                      @if(!empty($result))
                      @foreach($result as $key => $value)
                  <tr>
                    <td>{{$counter++}}</td>
                    <td>{{$value['className']}}</td>
                    <td>
                    <button type="button" class="btn bg-gradient-primary btn-sm" onclick="loadModal('/edit-class/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">Edit</button>
 |
                        <a href="{{ url('/delete-class/'.$value['id']) }}"><button type="button" class="btn bg-gradient-danger btn-sm">Delete</button>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
