@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        @if(Auth::user()->role == 0)
        <div class="row">          
          <div class="col-lg-12 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ @count($result)}}</h3>
                <p>Total Schools Registered</p>
              </div>
              <div class="icon">
                <i class="ion ion-home"></i>
              </div>
              <a href="{{ url('all-users') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        @endif
        @if(Auth::user()->role == 1)
        <div class="row">          
          <div class="col-lg-12 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ @count($classes)}}</h3>
                <p>Total Classes Registered</p>
              </div>
              <div class="icon">
                <i class="ion ion-home"></i>
              </div>
              <a href="{{ url('all-users') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        @endif
        <!-- /.row -->
        <!-- Main row -->
      
      </div><!-- /.container-fluid -->
@endsection
