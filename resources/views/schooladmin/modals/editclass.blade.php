<div class="modal-header">
    <h4 class="modal-title">Update {{ @$result[0]['className']}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/update-class/'.@$result[0]['id']) }}" id="classRegistrationUpdate">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="className">class Name</label>
                <input type="text" class="form-control" id="className" placeholder="Enter Class Name" required
                    name="className" value="{{ @$result[0]['className']}}" />
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    var id = "<?php echo $result[0]['id']; ?>";
    $.ajax({
        type: "POST",
        url: site_url + "/update-class/"+id,
        data:  $("#classRegistrationUpdate").serialize(),
        dataType: 'json',
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');
                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}
</script>