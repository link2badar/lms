<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>LMS | Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('public/admin/font/all.min.css') }}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('public/admin/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/responsive.bootstrap4.min.css') }}">

  <!-- Modals -->
  <link rel="stylesheet" href="{{ asset('public/admin/css/toastr.min.css') }}">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('public/admin/css/bootstrap-4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('public/admin/css/icheck-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/jqvmap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/summernote-bs4.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('dashboard') }}" class="brand-link">
      <span class="brand-text font-weight-light">Dashboard Control</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('public/admin/img/avatar3.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <!--  <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </a>
              </li>
            </ul>
          </li> -->
          
          <li class="nav-item">
            <a href="{{ url('dashboard') }}" class="nav-link {{ ($pageName == 'Dashboard') ? 'active' : '' }}">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @if(Auth::user()->role == 0)
          <li class="nav-item">
            <a href="{{ url('all-schools') }}" class="nav-link {{ ($pageName == 'All Schools') ? 'active' : '' }}">
              <i class="nav-icon fas fa-school"></i>
              <p>
                All Schools
              </p>
            </a>
          </li>
          @endif

          @if(Auth::user()->role == 1)
          <li class="nav-item">
            <a href="{{ url('all-instructors') }}" class="nav-link {{ ($pageName == 'All Instructors') ? 'active' : '' }}">
            <i class="fas fa-user"></i>
              <p>
                All Instructors
              </p>
            </a>
          </li>
          @endif
          @if(Auth::user()->role == 1)
          <li class="nav-item">
            <a href="{{ url('all-classes') }}" class="nav-link {{ ($pageName == 'All Classes') ? 'active' : '' }}">
            <i class="fas fa-archway"></i>
              <p>
                All Classes
              </p>
            </a>
          </li>
          @endif

          @if(Auth::user()->role == 1)
          <li class="nav-item">
            <a href="{{ url('all-courses') }}" class="nav-link {{ ($pageName == 'All Courses') ? 'active' : '' }}">
            <i class="fas fa-book"></i>
              <p>
                All Courses
              </p>
            </a>
          </li>
          @endif
          
          <li class="nav-item">
            <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="nav-link">
              <i class="fa fa-sign-out nav-item" style="font-size: 1.1rem;"></i>
              <p>
                Logout
              </p>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $pageName }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            @if(!empty($breadcrumbs))
            @foreach($breadcrumbs as $key => $val)
              <li class="breadcrumb-item"><a href="{{ url($key) }}">{{$val}}</a></li>
              @endforeach
              @endif
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020-2021 <a href="{{ url('/')}}">Blog</a>.</strong>
    All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

<script src="{{ asset('public/admin/js/jquery.min.js') }}"></script>
<script src="{{ asset('public/admin/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery-ui.min.js') }}"></script>
<script>
  var site_url = "{{ url('/') }}";
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('public/admin/js/Chart.min.js') }}"></script>
<script src="{{ asset('public/admin/js/sparkline.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.knob.min.js') }}"></script>

<script src="{{ asset('public/admin/js/moment.min.js') }}"></script>
<script src="{{ asset('public/admin/js/daterangepicker.js') }}"></script>
<script src="{{ asset('public/admin/js/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.overlayScrollbars.min.js') }}"></script>

<!-- SweetAlert2 -->
<script src="{{ asset('public/admin/js/sweetalert2.min.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('public/admin/js/toastr.min.js') }}"></script>

<script src="{{ asset('public/admin/js/adminlte.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('public/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('public/admin/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('public/admin/js/dashboard.js') }}"></script>
<script src="{{ asset('public/admin/js/demo.js') }}"></script>
<script src="{{ asset('public/admin/js/custom.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

</body>
</html>
