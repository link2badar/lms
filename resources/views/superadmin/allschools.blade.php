@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Schools</h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                        <button type="button" class="btn btn-primary" onclick="loadModal( '/register-new-school')" data-toggle="modal" data-target="#modal-lg">
                            Register New School
                        </button>
                    </li>
                  </ul>
                </div>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Admin</th>
                    <th>Email</th>
                    <th>Phone #</th>
                    <th>Address</th>
                    <th>Detail</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php $counter = 1; ?>
                      @if(!empty($result))
                      @foreach($result as $key => $value)
                  <tr>
                    <td>{{$counter++}}</td>
                    <td>{{$value['schoolName']}}</td>
                    <td>{{ $value['adminName']}}</td>
                    <td>{{$value['schoolEmail']}}</td>
                    <td>{{$value['schoolPhone']}}</td>
                    <td>{{$value['schoolAddress']}}</td>
                    <td><button class="btn btn-block btn-outline-success btn-sm" onclick="loadModal('/school/detail/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">{{'more detail'}}</button></td>
                    
                    <td>
                    <button type="button" class="btn bg-gradient-primary btn-sm" onclick="loadModal('/edit-school/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">Edit</button>
 |
                        <a href="{{ url('/delete-school/'.$value['id']) }}"><button type="button" class="btn bg-gradient-danger btn-sm">Delete</button>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
