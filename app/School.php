<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class School extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'school';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'schoolName', 'adminName', 'schoolEmail', 'schoolPass', 'schoolPhone', 'schoolAddress', 'totalClasses', 'totalStudents', 'addedBy'
    ];
}