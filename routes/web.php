<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//---------------Dashboard---------------------
Route::get('/dashboard', 'DashboardController@index');

//---------------Schools---------------------
Route::get('/all-schools', 'SchoolController@index');
Route::any('/register-new-school', 'SchoolController@add');
Route::any('/save-new-school', 'SchoolController@save');
Route::any('/delete-school/{id}', 'SchoolController@delete');
Route::any('/edit-school/{id}', 'SchoolController@edit');
Route::any('/update-school/{id}', 'SchoolController@update');
Route::any('/school/detail/{id}', 'SchoolController@detail');

//---------------Classes---------------------
Route::get('/all-classes', 'ClassController@index');
Route::any('/register-new-class', 'ClassController@add');
Route::any('/save-new-class', 'ClassController@save');
Route::any('/delete-class/{id}', 'ClassController@delete');
Route::any('/edit-class/{id}', 'ClassController@edit');
Route::any('/update-class/{id}', 'ClassController@update');
Route::any('/class/detail/{id}', 'ClassController@detail');

//---------------Courses---------------------
Route::get('/all-courses', 'CourseController@index');
Route::any('/register-new-course', 'CourseController@add');
Route::any('/save-new-course', 'CourseController@save');
Route::any('/delete-course/{id}', 'CourseController@delete');
Route::any('/edit-course/{id}', 'CourseController@edit');
Route::any('/update-course/{id}', 'CourseController@update');
Route::any('/course/detail/{id}', 'CourseController@detail');


//---------------Instructors---------------------
Route::get('/all-instructors', 'InstructorController@index');
Route::any('/register-new-instructor', 'InstructorController@add');
Route::any('/save-new-instructor', 'InstructorController@save');
Route::any('/delete-instructor/{id}', 'InstructorController@delete');
Route::any('/edit-instructor/{id}', 'InstructorController@edit');
Route::any('/update-instructor/{id}', 'InstructorController@update');
Route::any('/instructor/detail/{id}', 'InstructorController@detail');