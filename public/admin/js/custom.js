function loadModal(url) {
    $.ajax({
        url: site_url + url,
        type: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        success: function (result) {
            $(".modal-content").html(result);
        },
    });
}
